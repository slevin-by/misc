#include <iostream>
 
unsigned long long getLFSR(unsigned long long &reg)
{
        unsigned long long newBit = ((reg >> 62) ^ (reg >> 60) ^
				     (reg >> 46) ^ (reg >> 39) ^
				     (reg >> 21) ^ (reg >> 19) ^
				     (reg >> 14) ^ (reg >> 10) ^
				     (reg >> 7) ^ (reg >> 2) ^
				     (reg >> 1)) & 0x00000001;
        newBit <<= 63;
        reg >>= 1;
	reg |= newBit;
        return reg;
}
 
int main()
{
	unsigned long long reg;
	
	std::cin >> reg;
 
 	for (int i = 0; i < 100; i++)
    	{
        	std::cout << getLFSR(reg) << std::endl;
    	}
 
    	return 0;
}
