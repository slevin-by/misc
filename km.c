#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int sfw_find_module(void)
{   
    int is_loaded = 0; 
    FILE *pfd = popen("lsmod | grep sfw_driver.ko", "r");
    char buf[16];
    // if there is some result the module must be loaded
    if (fread(buf, 1, sizeof(buf), pfd) > 0)
        is_loaded = 1;
    fclose(pfd);
    return is_loaded;
}

int sfw_load_driver(void)
{
    system("insmod sfw_driver.ko");
    return sfw_find_module();
}

int sfw_unload_driver()
{
    system("rmmod sfw_driver.ko");
    return !sfw_find_module();
}

int sfw_reload_driver()
{
    if (sfw_find_module())
        return sfw_unload_driver();
    return sfw_load_driver();
}

const char *sfw_get_homedir(void)
{
    struct passwd *pw = getpwuid(getuid());
    return pw->pw_dir;
}

const char *sfw_get_init_path(void)
{
    const char *homedir = sfw_get_homedir();
    char *path = (char *)calloc(256, sizeof(char));
    strncpy(path, homedir, 255);
    strncat(path, "/.sfwrules", 255);
    return path;
}

int main(void)
{
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
    
    printf("%s\n", homedir);
    
    char path[256];
    strncpy(path, homedir, 255);
    strncat(path, "/.sfwrules", 255);
    
    printf("%s\n", path);
    
    FILE *fd = fopen(path, "w");
    if (!fd)
        perror(path);
    else
    {
        printf("%s is opened!\n", path);
        fprintf(fd, "%s", "hello");
    }
    fclose(fd);
    
    printf("func: %s\n", sfw_get_init_path());
    
    int ret = system("touch ~/workspace/lo1l");
    if (ret == -1)
        printf("err1\n");
    perror("touch");
        
    ret = system("insmod hehe.ko");
    if (ret == -1)
        printf("err2\n");
    perror("insmod");
    
    return 0;
}
