format ELF64

public main

extrn printf

main:
    mov rbp, rsp; for correct debugging
    mov ax, cs
    mov ds, ax
    
run:
    mov rsi, [codeptr]
    lodsb
    add qword [codeptr], 1
    cmp al, '>'
    je incptr
    cmp al, '<'
    je decptr
    cmp al, '+'
    je incdata
    cmp al, '-'
    je decdata
    cmp al, '.'
    je outchar
    cmp al, ','
    je inchar
    cmp al, '['
    je startloop
    cmp al, ']'
    je endloop
    
    jmp exit
    
incptr:
    add qword [dataptr], 8
    jmp run
decptr:
    sub qword [dataptr], 8
    jmp run
incdata:
    mov rax, [dataptr]
    mov rsi, rax
    mov rdi, rax
    lodsq
    add rax, 1
    stosq
    jmp run
decdata:
    mov rax, [dataptr]
    mov rsi, rax
    mov rdi, rax
    lodsq
    sub rax, 1
    stosq
    jmp run
outchar:
    mov rsi, [dataptr]
    lodsq
    mov rdx, rax
    call OutChar
    jmp run
inchar:
    ;mov rdi, [dataptr]
    ;xor rax, rax
    ;call b_input_key_wait
    ;stosq
    jmp run
startloop:
    mov rsi, [dataptr]
    lodsq
    cmp rax, 0
    jne run
    mov rsi, [codeptr]
startloop_next:
    lodsb
    cmp al, ']'
    jne startloop_next
    mov qword [codeptr], rsi
    jmp run
endloop:
    xchg bx, bx
    mov rsi, [dataptr]
    lodsq
    cmp rax, 0
    je run
    mov rsi, [codeptr]
    std
endloop_next:
    lodsb
    cmp al, '['
    jne endloop_next
    add rsi, 1
    mov qword [codeptr], rsi
    cld
    jmp run

exit:
    xor rax, rax
    ret
    
OutChar:    ; rdx - number
    sub rsp, 32
    and rsp, -16
    mov rcx, out_char_format
    call printf
    mov rsp, rbp
    jmp run
    
out_char_format:    db    '%c',0

codeptr:    dq    _code
dataptr:    dq    _data

_code:
db    '>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.>>>++++++++[<++++>-]<.>>>++++++++++[<+++++++++>-]<---.<<<<.+++.------.--------.>>+.',0

_data:
    