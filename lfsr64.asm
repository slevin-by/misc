format ELF64

section '.data'
    out_ulong_format    db    "%llu",0

section '.text' executable

extrn printf

public main
main:
    
    mov rbp, rsp
    
    mov rax, 100
    ; LFSR starts
    mov rbx, rax
    sar rbx, 62
    
    mov rcx, rax
    sar rcx, 60
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 46
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 39
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 21
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 19
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 14
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 10
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 7
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 2
    
    xor rbx, rcx
    mov rcx, rax
    sar rcx, 1
    
    xor rbx, rcx
    and rbx, 1
    sal rbx, 63
    sar rax, 1
    or rax, rbx    
    ; LFSR ends
    
    mov rdx, rax
    call OutULong
    
    xor eax, eax
    ret
    
OutULong:    ; rdx - number
    sub rsp, 32
    and rsp, -16
    mov rcx, out_ulong_format
    call printf
    mov rsp, rbp
    ret
    