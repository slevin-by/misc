#!/bin/python

from math import *
import re

dict = {'(': 0, ')': 0, '+': 1, '-': 1, '*': 2, '/': 2, '%': 2, '//': 2, '**': 3, '^': 3, '&': 3, '|': 3}
operators = ['+', '-', '/', '*', '%', '//', '**', '^', '&', '|']
binFuncs = ['copysign', 'fmod', 'ldexp', 'pow', 'atan2', 'hypot']

class Stack:
     def __init__(self):
         self.items = []
     def isEmpty(self):
         return self.items == []
     def push(self, item):
         self.items.append(item)
     def pop(self):
         return self.items.pop()
     def peek(self):
         return self.items[len(self.items)-1]
     def size(self):
         return len(self.items)

def isOperand(char):
    if (char >= '0' and char <= '9'):
        return True
    else:
        return False

def isPartOfFunc(char):
    if (char >= 'a' and char <= 'z'):
        return True
    else:
        return False

def correctMultiplication(exp):
    exp = exp.replace(")(", ")*(")

    ultimateExpr = ""
    end = False
    i = 0
    ch1 = exp[0]
    ultimateExpr = ultimateExpr + ch1
    while not end:
        if i == len(exp)-1:
            break
        i = i + 1
        ch2 = exp[i]

        if (ch1 == ')' and isOperand(ch2)) or (isOperand(ch1) and ch2 == '(') or \
           (ch1 == ')' and isPartOfFunc(ch2)) or (isOperand(ch1) and isPartOfFunc(ch2)):
            ultimateExpr = ultimateExpr + '*' + ch2
        else:
            ultimateExpr = ultimateExpr + ch2

        if i < (len(exp)-1):
            #i = i + 1
            ch1 = ch2
        else:
            end = True
    ultimateExpr = ultimateExpr.replace("atan2*(", "atan2(")

    ultimateExpr = re.sub("-([a-z])", r"-1*\1", ultimateExpr)       # -log ==> -1*log

    ultimateExpr = re.sub("([a-z])(\d+)(\+|-|\*|\/|%|\^|\&|\||$)", r"\1(\2)\3", ultimateExpr)  # log10 ==> log(10)

    return ultimateExpr

def correctExponents(exp):
    #exp = re.sub("(\d)e",r"\1*e", exp)
    #exp = re.sub("e(\d)", r"e*\1", exp)
    exp = re.sub("e", "2.718281828459045", exp)
    return exp

def correctFunctionBrackets(exp):
    for operator in operators:
        exp = re.sub("([a-z]+)(\d)#operator", r"\1(\2)(\3)", exp)
    return exp

#def correctExpt(exp):
#    return exp.replace("**", "^")

def correctMinus(exp):
    exp = re.sub("\-\-", r"+", exp)

    exp = re.sub("\++", r"+", exp)
    exp = re.sub("^\+", r"", exp)

    exp = re.sub("\-\+", r"-", exp)
    exp = re.sub("\+\-", r"-", exp)

    exp = re.sub("\-\-", r"+", exp)

    exp = re.sub("\++", r"+", exp)
    exp = re.sub("^\+", r"", exp)

    exp = exp.replace("-(", "-1*(")

    return exp

def inf2postf(exp):
    stack = Stack()
    res = []

    exp = exp.replace(" ", "")
    exp = correctMinus(exp)
    #exp = correctExpt(exp)
    exp = correctMultiplication(exp)
    exp = correctExponents(exp)

    end = False
    i = 0

    while not end:
        minus = False
        c = exp[i]

        if c == '-' and (exp[i-1] != ')' or i == 0):
            minus = True
            i = i + 1
            c = exp[i]

        x = None
        while c >= '0' and c <= '9':
            if x:
                x = x*10 + int(c)
            else:
                x = int(c)
            i = i + 1
            if i < len(exp):
                c = exp[i]
            else:
                break
        else:
            if c == '.':
                    i = i + 1
                    regExp = re.compile('\d+\.([^\d]|$)')
                    if i < len(exp):
                        regStr = str(x) + str(c) + str(exp[i])
                    else:
                        regStr = str(x) + str(c)

                    if x and regExp.match(regStr):
                        x = float(x)
                        if i == len(exp):
                            res.append(x)
                            while not stack.isEmpty():
                                res.append(stack.pop())
                            break
                        else:
                            c = exp[i]
                    else:
                        y = None
                        cnt = 1
                        #i = i + 1
                        c = exp[i]
                        while c >= '0' and c <= '9':
                            if y != None:
                                y = y*10 + float(c)
                                cnt = cnt + 1
                            else:
                                y = float(c)
                            i = i + 1
                            if i < len(exp):
                                c = exp[i]
                            else:
                                break
                        y = float(y / (10**cnt))
                        if not x:
                            x = 0
                        x = float(x + y)
        if minus:
            x = 0 - x

        f = ""
        if c >= 'a' and c <= 'z':
            while c != '(':
                #if c >= '0' and c <= '9':
                #    if c == '2' and f == 'atan':
                #        pass
                #    else:
                #        i = i - 1
                #        break
                f += c
                i = i + 1
                c = exp[i]
        if f:
            stack.push(f)

        if x != None:
            res.append(x)

        if c == '(':
            stack.push(c)
        elif c == ')':
            while stack.peek() != '(':
                res.append(stack.pop())
            stack.pop()
            if (not stack.isEmpty()) and (stack.peek() >= 'a') and (stack.peek() <= 'z'):
                res.append(stack.pop())
            #if i < len(exp)-1 and (exp[i+1] >= '0') and (exp[i+1] <= '9'):     ######
            #    stack.push("*")
        elif c in operators:
            if not stack.isEmpty():
                check = stack.peek()
                if (check[0] >= 'a') and (check[0] <= 'z'):
                    pass
                if dict[check] >= dict[c]:
                    while (not stack.isEmpty()) and (stack.peek() != '(') and (dict[stack.peek()] >= dict[c]):
                        res.append(stack.pop())
                if c == exp[i+1]:       # for '**' and '//'
                    stack.push(str(c)+str(exp[i+1]))
                    i = i + 1
                else:
                    stack.push(c)
            else:
                if c == exp[i+1]:       # for '**' and '//'
                    stack.push(str(c)+str(exp[i+1]))
                    i = i + 1
                else:
                    stack.push(c)

        i = i + 1
        if i >= len(exp):
            while not stack.isEmpty():
                res.append(stack.pop())
            end = True
    return res

def calculate(calc):
    N = len(calc)
    if N == 1:
        return calc[0]
    else:
        i = 0
        result = 0
        op = []
        opN = 0

        while i < N:
            regFunc = re.compile('[a-z]')
            while (calc[i] not in operators) and (not regFunc.match(str(calc[i]))):
                op.append(calc[i])
                opN = opN + 1
                i = i + 1

            if calc[i] in operators:
                s = str(op[opN-2])+str(calc[i])+str(op[opN-1])
                result = eval(s)
                #if calc[i] == '+':
                #    result = op[opN-2] + op[opN-1]
                #elif calc[i] == '-':
                #    result = op[opN-2] - op[opN-1]
                #elif calc[i] == '/':
                #    result = (op[opN-2] / op[opN-1])
                #elif calc[i] == '*':
                #    result = op[opN-2] * op[opN-1]
                #elif calc[i] == '%':
                #    result = (op[opN-2] % op[opN-1])
                #elif calc[i] == '^':
                #    result = op[opN-2] ** op[opN-1]

                del op[opN-1]
                del op[opN-2]
            elif calc[i] not in binFuncs:
                expression = ""
                expression = expression + str(calc[i]) + "(" + str(op[opN-1]) + ")"
                result = eval(expression)
                del op[opN-1]
                opN = opN + 1
            else:
                expression = ""
                expression = expression + str(calc[i]) + "(" + str(op[opN-2]) + "," + str(op[opN-1]) + ")"
                result = eval(expression)
                del op[opN-1]
                del op[opN-2]

            op.append(result)

            opN = opN - 1
            i = i + 1

        return result

# start
expr = raw_input()
clc = inf2postf(expr)
print clc
print expr, "=", calculate(clc)
