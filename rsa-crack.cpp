#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void usage()
{
	printf("Wrong amount of arguments.\n");
	printf("Uses: crackRSA -n <n> -e   <e>\n");
	printf("      crackRSA -e <e> -phi <phi>\n");
}

int main(int argc, char **argv)
{	
	unsigned long long n = 0, e = 0, phi = 0;
	unsigned long long d = 0;

	if (argc < 4)
	{
		usage();
		exit(0);
	}
	else
	{
		int i = 1;
		while (i < argc)
		{
			if (!strcmp("-n", argv[i]))
				n = atoi(argv[++i]);
			if (!strcmp("-e", argv[i]))
				e = atoi(argv[++i]);
			if (!strcmp("-phi", argv[i]))
				phi = atoi(argv[++i]);
			i++;
		}

		if (!((n != 0 && e != 0) || (e != 0 && phi != 0)))
		{
			usage();
			exit(0);
		}
	}

	//n = 21583;
	//e = 20787;
	//phi = 21280;

	if (0 == phi)
	{
		unsigned long long res;
		unsigned long long p = sqrt((double)n);
		
		while ((res = n % p) != 0)
			p--;

		unsigned long long q = n / p;
		
		phi = (p - 1)*(q - 1);
	}
	
	while (((e * d) % phi) != 1)
		d++;

	printf("\n*** CRACKED ***\nd: %lld\n", d);

	return 0;
}
