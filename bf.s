// . : $46
// + : $43
// - : $45
// > : $62
//

.data
arg:
	.space 1024
	.set arg_length, . - arg - 1
cmpr:
	.space 1
format:
	.string "%c\n"
out:
	.long 0
length:
	.long 0

.globl main
main:
	
	call input
	//call print

//	movl $arg, %eax
//	movl $2, %edx
//	addl %edx, %eax
//	movb (%eax), %al
//	movsbl %al, %eax
//	push %eax
//	push $format
//	call printf
//	addl $8, %esp
	
//	movl $arg,%eax
//	movl 0(%eax),%ebx
//	movl %ebx,arg
//	call print
	
//	movl $0, %ecx
//begin:	movl $arg, %eax
//	movl $dot, %ebx
//	movl 4(%ebx), %ecx
//	cmp %ecx, 4(%eax)
//	jne exit
//	call print

	xor %ecx, %ecx
	movl $0, %edx
begin:
	movl $1024, %ebx
	cmpl %ebx, %edx
	je exit
	
	movl $arg, %eax
	call nextSymbol

	//movl $52, %ebx
	//cmpl %eax, %ebx
	//je 

	movl $43, %ebx
	cmpl %eax, %ebx
	je is_plus
	
	movl $46, %ebx
	cmpl %eax, %ebx
	je is_dot
next:
	incl %edx
	jmp begin
is_plus:
	//push %ecx
	//movl $out, %ecx
	addl $1, %ecx
	//movl %ecx, out
	//pop %ecx
	jmp next
is_dot:
	push %eax
	movl %ecx, %eax
	call printSymbol
	pop %eax
	jmp next
	
	
exit:	ret

.type input, @function
input:
	movl $3, %eax
	movl $2, %ebx
	movl $arg, %ecx
	movl $1024, %edx
	int $0x80

	ret
	
.type print, @function
print:
	fldpi
	movl $4, %eax
	movl $1, %ebx
	movl $arg, %ecx
	movl $arg_length, %edx
	int $0x80

	ret

.type nextSymbol, @function
// before:
// 	%eax - string
// 	%edx - index of required element
// after:
//	%eax - required symbol
nextSymbol:
	addl %edx, %eax
	movb (%eax), %al
	movsbl %al, %eax

	ret

.type printSymbol, @function
// %eax - required symbol
printSymbol:
	pusha
	push %eax
	push $format
	call printf
	addl $8, %esp
	popa
	ret
	
